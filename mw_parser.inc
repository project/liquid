<?php


require_once( 'mw_sanitizer.inc' );

$wgUrlProtocols = 'http:\/\/|https:\/\/|ftp:\/\/|irc:\/\/|gopher:\/\/|news:|mailto:';
// Constants needed for external link processing
define( 'HTTP_PROTOCOLS', 'http:\/\/|https:\/\/' );
// Everything except bracket, space, or control characters
define( 'EXT_LINK_URL_CLASS', '[^]<>"\\x00-\\x20\\x7F]' );
// Including space
define( 'EXT_LINK_TEXT_CLASS', '[^\]\\x00-\\x1F\\x7F]' );
define( 'EXT_IMAGE_FNAME_CLASS', '[A-Za-z0-9_.,~%\\-+&;#*?!=()@\\x80-\\xFF]' );
define( 'EXT_IMAGE_EXTENSIONS', 'gif|png|jpg|jpeg' );
define( 'EXT_LINK_BRACKETED',  '/\[(\b('.$wgUrlProtocols.')'.EXT_LINK_URL_CLASS.'+) *('.EXT_LINK_TEXT_CLASS.'*?)\]/S' );
define( 'EXT_IMAGE_REGEX',
	'/^('.HTTP_PROTOCOLS.')'.  # Protocol
	'('.EXT_LINK_URL_CLASS.'+)\\/'.  # Hostname and path
	'('.EXT_IMAGE_FNAME_CLASS.'+)\\.((?i)'.EXT_IMAGE_EXTENSIONS.')$/S' # Filename
);


class mw_parser
{
  // Cleared with clearState():
  var $mDTopen, $mStripState = array();
  var $mLastSection, $mInPre;
  var $mUniqPrefix;

  function clearState() {
    $this->mLastSection = '';
    $this->mDTopen = false;
    $this->mStripState = array();
    $this->mInPre = false;
    $this->mUniqPrefix = 'UNIQ' . mw_parser::getRandomString();
  }

  function parse($text) {
    $this->clearState();
    $this->mStripState = NULL;
    $x = & $this->mStripState;
    $text = $this->strip($text, $x);

    $text = $this->internalParse( $text );
    
    $text = $this->unstrip( $text, $this->mStripState );

    // Clean up special characters, only run once, next-to-last before doBlockLevels
    $fixtags = array(
		     // french spaces, last one Guillemet-left
		     // only if there is something before the space
		     '/(.) (?=\\?|:|;|!|\\302\\273)/' => '\\1&nbsp;\\2',
		     // french spaces, Guillemet-right
		     '/(\\302\\253) /' => '\\1&nbsp;',
		     '/<center *>(.*)<\\/center *>/i' => '<div class="center">\\1</div>',
		     );

    $text = preg_replace( array_keys($fixtags), array_values($fixtags), $text );

    // only once and last
    $text = $this->doBlockLevels( $text, true);

    $text = $this->unstripNoWiki( $text, $this->mStripState );

    return $text;
  }

  /**
   * Get a random string
   *
   * @access private
   * @static
   */
  function getRandomString() {
    return dechex(mt_rand(0, 0x7fffffff)) . dechex(mt_rand(0, 0x7fffffff));
  }
  
  /**
   * Replaces all occurrences of <$tag>content</$tag> in the text
   * with a random marker and returns the new text. the output parameter
   * $content will be an associative array filled with data on the form
   * $unique_marker => content.
   *
   * If $content is already set, the additional entries will be appended
   * If $tag is set to STRIP_COMMENTS, the function will extract
   * <!-- HTML comments -->
   *
   * @access private
   * @static
   */
  function extractTagsAndParams($tag, $text, &$content, &$tags, &$params, $uniq_prefix = ''){
    $rnd = $uniq_prefix . '-' . $tag . mw_parser::getRandomString();
    if ( !$content ) {
      $content = array( );
    }
    $n = 1;
    $stripped = '';
    
    if ( !$tags ) {
      $tags = array( );
    }
    
    if ( !$params ) {
      $params = array( );
    }
    
    if( $tag == STRIP_COMMENTS ) {
      $start = '/<!--()/';
      $end   = '/-->/';
    } else {
      $start = "/<$tag(\\s+[^>]*|\\s*)>/i";
      $end   = "/<\\/$tag\\s*>/i";
    }
    
    while ( '' != $text ) {
      $p = preg_split( $start, $text, 2, PREG_SPLIT_DELIM_CAPTURE );
      $stripped .= $p[0];
      if( count( $p ) < 3 ) {
	break;
      }
      $attributes = $p[1];
      $inside     = $p[2];
      
      $marker = $rnd . sprintf('%08X', $n++);
      $stripped .= $marker;
      
      $tags[$marker] = "<$tag$attributes>";
      $params[$marker] = Sanitizer::decodeTagAttributes( $attributes );
      
      $q = preg_split( $end, $inside, 2 );
      $content[$marker] = $q[0];
      if( count( $q ) < 2 ) {
# No end tag -- let it run out to the end of the text.
	break;
      } else {
	$text = $q[1];
      }
    }
    return $stripped;
  }
  
  /**
   * Wrapper function for extractTagsAndParams
   * for cases where $tags and $params isn't needed
   * i.e. where tags will never have params, like <nowiki>
   *
   * @access private
   * @static
   */
  function extractTags( $tag, $text, &$content, $uniq_prefix = '' ) {
    $dummy_tags = array();
    $dummy_params = array();
    
    return mw_parser::extractTagsAndParams( $tag, $text, $content,
					 $dummy_tags, $dummy_params, $uniq_prefix );
  }

  /**
   * Strips and renders nowiki, pre, math, hiero
   * If $render is set, performs necessary rendering operations on plugins
   * Returns the text, and fills an array with data needed in unstrip()
   * If the $state is already a valid strip state, it adds to the state
   *
   * @param bool $stripcomments when set, HTML comments <!-- like this -->
   *  will be stripped in addition to other tags. This is important
   *  for section editing, where these comments cause confusion when
   *  counting the sections in the wikisource
   *
   * @access private
   */
  function strip( $text, &$state, $stripcomments = false ) {
    $html_content = array();
    $nowiki_content = array();
    $math_content = array();
    $pre_content = array();
    $comment_content = array();
    $ext_content = array();
    $ext_tags = array();
    $ext_params = array();
    $gallery_content = array();
    
# Replace any instances of the placeholders
    $uniq_prefix = $this->mUniqPrefix;
#$text = str_replace( $uniq_prefix, wfHtmlEscapeFirst( $uniq_prefix ), $text );
    
# html
    global $wgRawHtml;
    if( $wgRawHtml ) {
      $text = mw_parser::extractTags('html', $text, $html_content, $uniq_prefix);
      foreach( $html_content as $marker => $content ) {
	$html_content[$marker] = $content;
      }
    }
    
# nowiki
    $text = mw_parser::extractTags('nowiki', $text, $nowiki_content, $uniq_prefix);
    foreach( $nowiki_content as $marker => $content ) {
      $nowiki_content[$marker] = wfEscapeHTMLTagsOnly( $content );
    }
    
# math
    $text = mw_parser::extractTags('math', $text, $math_content, $uniq_prefix);
    foreach( $math_content as $marker => $content ){
      $math_content[$marker] = renderMath( $content );
    }
    
# pre
    $text = mw_parser::extractTags('pre', $text, $pre_content, $uniq_prefix);
    foreach( $pre_content as $marker => $content ){
      $pre_content[$marker] = '<pre>' . wfEscapeHTMLTagsOnly( $content ) . '</pre>';
    }
    
# Comments
    if($stripcomments) {
      $text = mw_parser::extractTags(STRIP_COMMENTS, $text, $comment_content, $uniq_prefix);
      foreach( $comment_content as $marker => $content ){
	$comment_content[$marker] = '<!--'.$content.'-->';
      }
    }
   
# Merge state with the pre-existing state, if there is one
    if ( $state ) {
      $state['html'] = $state['html'] + $html_content;
      $state['nowiki'] = $state['nowiki'] + $nowiki_content;
      $state['math'] = $state['math'] + $math_content;
      $state['pre'] = $state['pre'] + $pre_content;
      $state['comment'] = $state['comment'] + $comment_content;
      
    } else {
      $state = array(
		     'html' => $html_content,
		     'nowiki' => $nowiki_content,
		     'math' => $math_content,
		     'pre' => $pre_content,
		     'comment' => $comment_content,
		     );
    }
    return $text;
  }
  
  /**
   * restores pre, math, and hiero removed by strip()
   *
   * always call unstripNoWiki() after this one
   * @access private
   */
  function unstrip( $text, &$state ) {
# Must expand in reverse order, otherwise nested tags will be corrupted
    foreach( array_reverse( $state, true ) as $tag => $contentDict ) {
      if( $tag != 'nowiki' && $tag != 'html' ) {
	foreach( array_reverse( $contentDict, true ) as $uniq => $content ) {
	  $text = str_replace( $uniq, $content, $text );
	}
      }
    }
    
    return $text;
  }
  
  /**
   * always call this after unstrip() to preserve the order
   *
   * @access private
   */
  function unstripNoWiki( $text, &$state ) {
# Must expand in reverse order, otherwise nested tags will be corrupted
    for ( $content = end($state['nowiki']); $content !== false; $content = prev( $state['nowiki'] ) ) {
      $text = str_replace( key( $state['nowiki'] ), $content, $text );
    }
    
    return $text;
  }
  
  /**
   * Helper function for parse() that transforms wiki markup into
   * HTML. Only called for $mOutputType == OT_HTML.
   *
   * @access private
   */
  function internalParse( $text ) {
    $args = array();

    $text = strtr( $text, array( '<noinclude>' => '', '</noinclude>' => '') );
    $text = preg_replace( '/<includeonly>.*?<\/includeonly>/s', '', $text );
    
    $text = Sanitizer::removeHTMLtags( $text, array( &$this, 'attributeStripCallback' ) );
    
    $text = preg_replace( '/(^|\n)-----*/', '\\1<hr />', $text );
    
    $text = $this->doHeadings( $text );
    $text = $this->doAllQuotes( $text );
    $text = $this->replaceInternalLinks( $text );
    $text = $this->replaceExternalLinks( $text );
   
    # replaceInternalLinks may sometimes leave behind
    # absolute URLs, which have to be masked to hide them from replaceExternalLinks
    $text = str_replace($this->mUniqPrefix."NOPARSE", "", $text);
    
    $text = $this->doTableStuff( $text );
//		$text = $this->formatHeadings( $text, $isMain );
    
    $regex = '/<!--IW_TRANSCLUDE (\d+)-->/';
    $text = preg_replace_callback($regex, array(&$this, 'scarySubstitution'), $text);
    
    return $text;
  }

  /**
   * Callback from the Sanitizer for expanding items found in HTML attribute
   * values, so they can be safely tested and escaped.
   * @param string $text
   * @param array $args
   * @return string
   * @access private
   */
  function attributeStripCallback( &$text, $args ) {
    $text = $this->unstripForHTML( $text );
    return $text;
  }
	
  function unstripForHTML( $text ) {
    $text = $this->unstrip( $text, $this->mStripState );
    $text = $this->unstripNoWiki( $text, $this->mStripState );
    return $text;
  }

  function legalChars() {
    $set = " %!\"$&'()*,\\-.\\/0-9:;=?@A-Z\\\\^_`a-z~\\x80-\\xFF";
    return $set;
  }

  /**
   * Parse headers and return html
   *
   * @access private
   */
  function doHeadings( $text ) {
    for ( $i = 6; $i >= 1; --$i ) {
      $h = substr( '======', 0, $i );
      $text = preg_replace( "/^{$h}(.+){$h}(\\s|$)/m",
			    "<h{$i}>\\1</h{$i}>\\2", $text );
    }
    return $text;
  }

  /**
   * Replace single quotes with HTML markup
   * @access private
   * @return string the altered text
   */
  function doAllQuotes( $text ) {
    $outtext = '';
    $lines = explode( "\n", $text );
    foreach ( $lines as $line ) {
      $outtext .= $this->doQuotes ( $line ) . "\n";
    }
    $outtext = substr($outtext, 0,-1);
    return $outtext;
  }

  /**
   * Helper function for doAllQuotes()
   * @access private
   */
  function doQuotes( $text ) {
    $arr = preg_split( "/(''+)/", $text, -1, PREG_SPLIT_DELIM_CAPTURE );
    if ( count( $arr ) == 1 )
      return $text;
    else
      {
        # First, do some preliminary work. This may shift some apostrophes from
        # being mark-up to being text. It also counts the number of occurrences 
        # of bold and italics mark-ups.
	$i = 0;
	$numbold = 0;
	$numitalics = 0;
	foreach ( $arr as $r ) {
	  if ( ( $i % 2 ) == 1 ) {
            # If there are ever four apostrophes, assume the first is supposed to
            # be text, and the remaining three constitute mark-up for bold text.
	    if ( strlen( $arr[$i] ) == 4 ) {
	      $arr[$i-1] .= "'";
	      $arr[$i] = "'''";
	    } else if ( strlen( $arr[$i] ) > 5 ) {
	      $arr[$i-1] .= str_repeat( "'", strlen( $arr[$i] ) - 5 );
	      $arr[$i] = "'''''";
	    }
# Count the number of occurrences of bold and italics mark-ups.
# We are not counting sequences of five apostrophes.
	    if ( strlen( $arr[$i] ) == 2 ) $numitalics++;  else
	      if ( strlen( $arr[$i] ) == 3 ) $numbold++;     else
		if ( strlen( $arr[$i] ) == 5 ) { $numitalics++; $numbold++; }
	  }
	  $i++;
	}
	
# If there is an odd number of both bold and italics, it is likely
# that one of the bold ones was meant to be an apostrophe followed
# by italics. Which one we cannot know for certain, but it is more
# likely to be one that has a single-letter word before it.
	if ( ( $numbold % 2 == 1 ) && ( $numitalics % 2 == 1 ) )
	  {
	    $i = 0;
	    $firstsingleletterword = -1;
	    $firstmultiletterword = -1;
	    $firstspace = -1;
	    foreach ( $arr as $r )
	      {
		if ( ( $i % 2 == 1 ) and ( strlen( $r ) == 3 ) )
		  {
		    $x1 = substr ($arr[$i-1], -1);
		    $x2 = substr ($arr[$i-1], -2, 1);
		    if ($x1 == ' ') {
		      if ($firstspace == -1) $firstspace = $i;
		    } else if ($x2 == ' ') {
		      if ($firstsingleletterword == -1) $firstsingleletterword = $i;
		    } else {
		      if ($firstmultiletterword == -1) $firstmultiletterword = $i;
		    }
		  }
		$i++;
	      }

# If there is a single-letter word, use it!
	    if ($firstsingleletterword > -1)
	      {
		$arr [ $firstsingleletterword ] = "''";
		$arr [ $firstsingleletterword-1 ] .= "'";
	      }
	    else if ($firstmultiletterword > -1)
	      {
		$arr [ $firstmultiletterword ] = "''";
		$arr [ $firstmultiletterword-1 ] .= "'";
	      }
# ... otherwise use the first one that has neither.
# (notice that it is possible for all three to be -1 if, for example,
# there is only one pentuple-apostrophe in the line)
	    else if ($firstspace > -1)
	      {
		$arr [ $firstspace ] = "''";
		$arr [ $firstspace-1 ] .= "'";
	      }
	  }
	
	
	$output = '';
	$buffer = '';
	$state = '';
	$i = 0;
	foreach ($arr as $r)
	  {
	    if (($i % 2) == 0)
	      {
		if ($state == 'both')
		  $buffer .= $r;
		else
		  $output .= $r;
	      }
	    else
	      {
		if (strlen ($r) == 2)
		  {
		    if ($state == 'i')
		      { $output .= '</i>'; $state = ''; }
		    else if ($state == 'bi')
		      { $output .= '</i>'; $state = 'b'; }
		    else if ($state == 'ib')
		      { $output .= '</b></i><b>'; $state = 'b'; }
		    else if ($state == 'both')
		      { $output .= '<b><i>'.$buffer.'</i>'; $state = 'b'; }
		    else # $state can be 'b' or ''
		      { $output .= '<i>'; $state .= 'i'; }
		  }
		else if (strlen ($r) == 3)
		  {
		    if ($state == 'b')
		      { $output .= '</b>'; $state = ''; }
		    else if ($state == 'bi')
		      { $output .= '</i></b><i>'; $state = 'i'; }
		    else if ($state == 'ib')
		      { $output .= '</b>'; $state = 'i'; }
		    else if ($state == 'both')
		      { $output .= '<i><b>'.$buffer.'</b>'; $state = 'i'; }
		    else # $state can be 'i' or ''
		      { $output .= '<b>'; $state .= 'b'; }
		  }
		else if (strlen ($r) == 5)
		  {
		    if ($state == 'b')
		      { $output .= '</b><i>'; $state = 'i'; }
		    else if ($state == 'i')
		      { $output .= '</i><b>'; $state = 'b'; }
		    else if ($state == 'bi')
		      { $output .= '</i></b>'; $state = ''; }
		    else if ($state == 'ib')
		      { $output .= '</b></i>'; $state = ''; }
		    else if ($state == 'both')
		      { $output .= '<i><b>'.$buffer.'</b></i>'; $state = ''; }
		    else # ($state == '')
		      { $buffer = ''; $state = 'both'; }
		  }
	      }
	    $i++;
	  }
# Now close all remaining tags.  Notice that the order is important.
	if ($state == 'b' || $state == 'ib')
	  $output .= '</b>';
	if ($state == 'i' || $state == 'bi' || $state == 'ib')
	  $output .= '</i>';
	if ($state == 'bi')
	  $output .= '</b>';
	if ($state == 'both')
	  $output .= '<b><i>'.$buffer.'</i></b>';
	return $output;
      }
	}
  
  /**
   * parse the wiki syntax used to render tables
   *
   * @access private
   */
  function doTableStuff ( $t ) {
    $t = explode ( "\n" , $t ) ;
    $td = array () ;
    $ltd = array () ;
    $tr = array () ;
    $ltr = array () ;
    $indent_level = 0;
    foreach ( $t AS $k => $x )
      {
	$x = trim ( $x ) ;
	$fc = substr ( $x , 0 , 1 ) ;
	if ( preg_match( '/^(:*)\{\|(.*)$/', $x, $matches ) ) {
	  $indent_level = strlen( $matches[1] );
	  
	  $attributes = $this->unstripForHTML( $matches[2] );
	  
	  $t[$k] = str_repeat( '<dl><dd>', $indent_level ) .
	    '<table' . Sanitizer::fixTagAttributes ( $attributes, 'table' ) . '>' ;
	  array_push ( $td , false ) ;
	  array_push ( $ltd , '' ) ;
	  array_push ( $tr , false ) ;
	  array_push ( $ltr , '' ) ;
	}
	else if ( count ( $td ) == 0 ) { } // Don't do any of the following
	else if ( '|}' == substr ( $x , 0 , 2 ) ) {
	  $z = "</table>" . substr ( $x , 2);
	  $l = array_pop ( $ltd ) ;
	  if ( array_pop ( $tr ) ) $z = '</tr>' . $z ;
	  if ( array_pop ( $td ) ) $z = '</'.$l.'>' . $z ;
	  array_pop ( $ltr ) ;
	  $t[$k] = $z . str_repeat( '</dd></dl>', $indent_level );
	}
	else if ( '|-' == substr ( $x , 0 , 2 ) ) { // Allows for |---------------
	  $x = substr ( $x , 1 ) ;
	  while ( $x != '' && substr ( $x , 0 , 1 ) == '-' ) $x = substr ( $x , 1 ) ;
	  $z = '' ;
	  $l = array_pop ( $ltd ) ;
	  if ( array_pop ( $tr ) ) $z = '</tr>' . $z ;
	  if ( array_pop ( $td ) ) $z = '</'.$l.'>' . $z ;
	  array_pop ( $ltr ) ;
	  $t[$k] = $z ;
	  array_push ( $tr , false ) ;
	  array_push ( $td , false ) ;
	  array_push ( $ltd , '' ) ;
	  $attributes = $this->unstripForHTML( $x );
	  array_push ( $ltr , Sanitizer::fixTagAttributes ( $attributes, 'tr' ) ) ;
	}
	else if ( '|' == $fc || '!' == $fc || '|+' == substr ( $x , 0 , 2 ) ) { 
	  if ( '|+' == substr ( $x , 0 , 2 ) ) {
	    $fc = '+' ;
	    $x = substr ( $x , 1 ) ;
	  }
	  $after = substr ( $x , 1 ) ;
	  if ( $fc == '!' ) $after = str_replace ( '!!' , '||' , $after ) ;
	  $after = explode ( '||' , $after ) ;
	  $t[$k] = '' ;
	  
# Loop through each table cell
	  foreach ( $after AS $theline )
	    {
	      $z = '' ;
	      if ( $fc != '+' )
		{
		  $tra = array_pop ( $ltr ) ;
		  if ( !array_pop ( $tr ) ) $z = '<tr'.$tra.">\n" ;
		  array_push ( $tr , true ) ;
		  array_push ( $ltr , '' ) ;
		}
	      
	      $l = array_pop ( $ltd ) ;
	      if ( array_pop ( $td ) ) $z = '</'.$l.'>' . $z ;
	      if ( $fc == '|' ) $l = 'td' ;
	      else if ( $fc == '!' ) $l = 'th' ;
	      else if ( $fc == '+' ) $l = 'caption' ;
	      else $l = '' ;
	      array_push ( $ltd , $l ) ;
	      
# Cell parameters
	      $y = explode ( '|' , $theline , 2 ) ;
# Note that a '|' inside an invalid link should not
# be mistaken as delimiting cell parameters
	      if ( strpos( $y[0], '[[' ) !== false ) {
		$y = array ($theline);
	      }
	      if ( count ( $y ) == 1 )
		$y = "{$z}<{$l}>{$y[0]}" ;
	      else {
		$attributes = $this->unstripForHTML( $y[0] );
		$y = "{$z}<{$l}".Sanitizer::fixTagAttributes($attributes, $l).">{$y[1]}" ;
	      }
	      $t[$k] .= $y ;
	      array_push ( $td , true ) ;
	    }
	}
      }
    
# Closing open td, tr && table
    while ( count ( $td ) > 0 )
      {
	if ( array_pop ( $td ) ) $t[] = '</td>' ;
	if ( array_pop ( $tr ) ) $t[] = '</tr>' ;
			$t[] = '</table>' ;
      }
    
    $t = implode ( "\n" , $t ) ;
    //wfProfileOut( $fname );
    return $t ;
  }

  function scarySubstitution($matches) {
    return $this->mIWTransData[(int)$matches[0]];
  }

  /**
   * Process [[ ]] wikilinks
   *
   * @access private
   */
  function replaceInternalLinks( $s ) {
    $wgUrlProtocols = 'http:\/\/|https:\/\/|ftp:\/\/|irc:\/\/|gopher:\/\/|news:|mailto:';
    static $tc = FALSE;
    if ( !$tc ) { $tc = $this->legalChars() . '#%'; }

    //split the entire text string on occurences of [[
    $a = explode( '[[', ' ' . $s );
    //get the first element (all text up to first [[), and remove the space we added
    $s = array_shift( $a );
    $s = substr( $s, 1 );

    // Match a link having the form [[namespace:link|alternate]]trail
    static $e1 = FALSE;
    if ( !$e1 ) { $e1 = "/^([{$tc}]+)(?:\\|(.+?))?]](.*)\$/sD"; }
    // Loop for each link
    for ($k = 0; isset( $a[$k] ); $k++) {
      $line = $a[$k];
      if ( preg_match( $e1, $line, $m ) ) {// page with normal text or alt
	$text = $m[2];
	// If we get a ] at the beginning of $m[3] that means we have a link that's something like:
	// [[Image:Foo.jpg|[http://example.com desc]]] <- having three ] in a row fucks up,
	// the real problem is with the $e1 regex
	// See bug 1300.
	//
	// Still some problems for cases where the ] is meant to be outside punctuation,
	// and no image is in sight. See bug 2095.

	if( $text !== '' && preg_match( "/^\](.*)/s", $m[3], $n ) ) {
	  $text .= ']'; // so that replaceExternalLinks($text) works later
	  $m[3] = $n[1];
	}
	// fix up urlencoded title texts
	if(preg_match('/%/', $m[1] )) $m[1] = urldecode($m[1]);
	$trail = $m[3];
      } else { // Invalid form; output directly
	$s .= $prefix . '[[' . $line ;
	continue;
      }
      
      // Don't allow internal links to pages containing
      // PROTO: where PROTO is a valid URL protocol; these
      // should be external links.
      if (preg_match('/^(\b(?:'.$wgUrlProtocols.'))/', $m[1])) {
	$s .= $prefix . '[[' . $line ;
	continue;
      }

      // Make subpage if necessary
      
      /*if( $useSubpages ) {
	$link = $this->maybeDoSubpageLink( $m[1], $text );
      } else {
	$link = $m[1];
      }*/
      $link = $m[1];

      $noforce = (substr($m[1], 0, 1) != ':');
      if (!$noforce) {
	// Strip off leading ':'
	$link = substr($link, 1);
      }
      
      
      $wasblank = ( '' == $text );
      if( $wasblank ) $text = $link;
      
      $wid = new WikiId($link);
      $s .= l($text, 'wiki/'.$wid->toURLString()) . $trail;
      
    }
    return $s;
  }

  /**
   * Replace external links
   *
   * Note: this is all very hackish and the order of execution matters a lot.
   * Make sure to run maintenance/parserTests.php if you change this code.
   *
   * @access private
   */
  function replaceExternalLinks( $text ) {
    $bits = preg_split( EXT_LINK_BRACKETED, $text, -1, PREG_SPLIT_DELIM_CAPTURE );

    $s = $this->replaceFreeExternalLinks( array_shift( $bits ) );
    
    $i = 0;
    while ( $i<count( $bits ) ) {
      $url = $bits[$i++];
      $protocol = $bits[$i++];
      $text = $bits[$i++];
      $trail = $bits[$i++];
      
      // The characters '<' and '>' (which were escaped by
      // removeHTMLtags()) should not be included in
      // URLs, per RFC 2396.
      if (preg_match('/&(lt|gt);/', $url, $m2, PREG_OFFSET_CAPTURE)) {
	$text = substr($url, $m2[0][1]) . ' ' . $text;
	$url = substr($url, 0, $m2[0][1]);
      }
      
      // If the link text is an image URL, replace it with an <img> tag
      // This happened by accident in the original parser, but some people used it extensively
      $img = $this->maybeMakeExternalImage( $text );
      if ( $img !== false ) {
	$text = $img;
      }
      
      $dtrail = '';
      
      // Set linktype for CSS - if URL==text, link is essentially free
      $linktype = ($text == $url) ? 'free' : 'text';
      
      // No link text, e.g. [http://domain.tld/some.link]
      if ( $text == '' ) {
	$text = htmlspecialchars( $url );
	$linktype = 'free';
      } else {
	// Have link text, e.g. [http://domain.tld/some.link text]s
	// Check for trail
	list( $dtrail, $trail ) = $this->splitTrail( $trail );
      }
      
      // Replace &amp; from obsolete syntax with &.
      // All HTML entities will be escaped by makeExternalLink()
      // or maybeMakeExternalImage()
      $url = str_replace( '&amp;', '&', $url );

      // Process the trail (i.e. everything after this link up until start of the next link),
      // replacing any non-bracketed links
      $trail = $this->replaceFreeExternalLinks( $trail );


      // Use the encoded URL
      // This means that users can paste URLs directly into the text
      // Funny characters like &ouml; aren't valid in URLs anyway
      // This was changed in August 2004
      $s .= ('<a href="'.url($url).'">'.$text.'</a>') . $dtrail . $trail;
    }
    return $s;
  }

  function splitTrail( $trail ) {
    static $regex = '/^([a-z]+)(.*)$/sD';
    $inside = '';
    if ( '' != $trail ) {
      if ( preg_match( $regex, $trail, $m ) ) {
	$inside = $m[1];
	$trail = $m[2];
      }
		}
    return array( $inside, $trail );
  }
  

  /**
   * Replace anything that looks like a URL with a link
   * @access private
   */
  function replaceFreeExternalLinks( $text ) {
    $wgUrlProtocols = 'http:\/\/|https:\/\/|ftp:\/\/|irc:\/\/|gopher:\/\/|news:|mailto:';

    $bits = preg_split( '/(\b(?:'.$wgUrlProtocols.'))/S', $text, -1, PREG_SPLIT_DELIM_CAPTURE );
    $s = array_shift( $bits );
    $i = 0;
    while ( $i < count( $bits ) ){
      $protocol = $bits[$i++];
      $remainder = $bits[$i++];
      
      if ( preg_match( '/^('.EXT_LINK_URL_CLASS.'+)(.*)$/s', $remainder, $m ) ) {
	// Found some characters after the protocol that look promising
	$url = $protocol . $m[1];
	$trail = $m[2];
	
	// The characters '<' and '>' (which were escaped by
	// removeHTMLtags()) should not be included in
	// URLs, per RFC 2396.
	if (preg_match('/&(lt|gt);/', $url, $m2, PREG_OFFSET_CAPTURE)) {
	  $trail = substr($url, $m2[0][1]) . $trail;
	  $url = substr($url, 0, $m2[0][1]);
	}
	
	// Move trailing punctuation to $trail
	$sep = ',;\.:!?';
	// If there is no left bracket, then consider right brackets fair game too
	if ( strpos( $url, '(' ) === false ) {
	  $sep .= ')';
	}
	
	$numSepChars = strspn( strrev( $url ), $sep );
	if ( $numSepChars ) {
	  $trail = substr( $url, -$numSepChars ) . $trail;
	  $url = substr( $url, 0, -$numSepChars );
	}
	
	// Replace &amp; from obsolete syntax with &.
	// All HTML entities will be escaped by makeExternalLink()
	// or maybeMakeExternalImage()
	$url = str_replace( '&amp;', '&', $url );
	
	// Is this an external image?
	$text = $this->maybeMakeExternalImage( $url );
	if ( $text === false ) {
	  //	  $text = "<a href='$url'>$url</a>";//l($url,$url);
	  $text = l($url,$url);
	}
	$s .= $text . $trail;
      } else {
	$s .= $protocol . $remainder;
      }
    }
    return $s;
  }

  /**
   * make an image if it's allowed
   * @access private
   */
  function maybeMakeExternalImage( $url ) {
    $text = false;
    if ( preg_match( EXT_IMAGE_REGEX, $url ) ) {
      // Image found
      $text = theme('image', htmlspecialchars( $url ), '', '', '', false);
    }
    return $text;
  }

  /**
   * Make lists from lines starting with ':', '*', '#', etc.
   *
   * @access private
   * @return string the lists rendered as HTML
   */
  function doBlockLevels( $text, $linestart ) {
    // Parsing through the text line by line.  The main thing
    // happening here is handling of block-level elements p, pre,
    // and making lists from lines starting with * # : etc.
    //
    $textLines = explode( "\n", $text );
    
    $lastPrefix = $output = '';
    $this->mDTopen = $inBlockElem = false;
    $prefixLength = 0;
    $paragraphStack = false;
    
    if ( !$linestart ) {
      $output .= array_shift( $textLines );
    }
    foreach ( $textLines as $oLine ) {
      $lastPrefixLength = strlen( $lastPrefix );
      $preCloseMatch = preg_match('/<\\/pre/i', $oLine );
      $preOpenMatch = preg_match('/<pre/i', $oLine );
      if ( !$this->mInPre ) {
	// Multiple prefixes may abut each other for nested lists.
	$prefixLength = strspn( $oLine, '*#:;' );
	$pref = substr( $oLine, 0, $prefixLength );
	
	// eh?
	$pref2 = str_replace( ';', ':', $pref );
	$t = substr( $oLine, $prefixLength );
	$this->mInPre = !empty($preOpenMatch);
      } else {
	// Don't interpret any other prefixes in preformatted text
	$prefixLength = 0;
	$pref = $pref2 = '';
	$t = $oLine;
      }
      
      // List generation
      if( $prefixLength && 0 == strcmp( $lastPrefix, $pref2 ) ) {
	// Same as the last item, so no need to deal with nesting or opening stuff
	$output .= $this->nextItem( substr( $pref, -1 ) );
	$paragraphStack = false;
	
	if ( substr( $pref, -1 ) == ';') {
	  // The one nasty exception: definition lists work like this:
	  // ; title : definition text
	  // So we check for : in the remainder text to split up the
	  // title and definition, without b0rking links.
	  $term = $t2 = '';
	  if ($this->findColonNoLinks($t, $term, $t2) !== false) {
	    $t = $t2;
	    $output .= $term . $this->nextItem( ':' );
	  }
	}
      } elseif( $prefixLength || $lastPrefixLength ) {
	// Either open or close a level...
	$commonPrefixLength = $this->getCommon( $pref, $lastPrefix );
	$paragraphStack = false;
	
	while( $commonPrefixLength < $lastPrefixLength ) {
	  $output .= $this->closeList( $lastPrefix{$lastPrefixLength-1} );
	  --$lastPrefixLength;
	}
	if ( $prefixLength <= $commonPrefixLength && $commonPrefixLength > 0 ) {
	  $output .= $this->nextItem( $pref{$commonPrefixLength-1} );
	}
	while ( $prefixLength > $commonPrefixLength ) {
	  $char = substr( $pref, $commonPrefixLength, 1 );
	  $output .= $this->openList( $char );
	  
	  if ( ';' == $char ) {
	    // FIXME: This is dupe of code above
	    if ($this->findColonNoLinks($t, $term, $t2) !== false) {
	      $t = $t2;
	      $output .= $term . $this->nextItem( ':' );
	    }
	  }
	  ++$commonPrefixLength;
	}
	$lastPrefix = $pref2;
      }
      if( 0 == $prefixLength ) {
	// No prefix (not in list)--go to paragraph mode
	// XXX: use a stack for nestable elements like span, table and div
	$openmatch = preg_match('/(<table|<blockquote|<h1|<h2|<h3|<h4|<h5|<h6|<pre|<tr|<p|<ul|<li|<\\/tr|<\\/td|<\\/th)/iS', $t );
	$closematch = preg_match('/(<\\/table|<\\/blockquote|<\\/h1|<\\/h2|<\\/h3|<\\/h4|<\\/h5|<\\/h6|'.
				 '<td|<th|<div|<\\/div|<hr|<\\/pre|<\\/p|'.$this->mUniqPrefix.'-pre|<\\/li|<\\/ul)/iS', $t );
	if ( $openmatch or $closematch ) {
	  $paragraphStack = false;
	  $output .= $this->closeParagraph();
	  if ( $preOpenMatch and !$preCloseMatch ) {
	    $this->mInPre = true;
	  }
	  if ( $closematch ) {
	    $inBlockElem = false;
	  } else {
	    $inBlockElem = true;
	  }
	} else if ( !$inBlockElem && !$this->mInPre ) {
	  if ( ' ' == $t{0} and ( $this->mLastSection == 'pre' or trim($t) != '' ) ) {
	    // pre
	    if ($this->mLastSection != 'pre') {
	      $paragraphStack = false;
	      $output .= $this->closeParagraph().'<pre>';
	      $this->mLastSection = 'pre';
	    }
	    $t = substr( $t, 1 );
	  } else {
	    // paragraph
	    if ( '' == trim($t) ) {
	      if ( $paragraphStack ) {
		$output .= $paragraphStack.'<br />';
		$paragraphStack = false;
		$this->mLastSection = 'p';
	      } else {
		if ($this->mLastSection != 'p' ) {
		  $output .= $this->closeParagraph();
		  $this->mLastSection = '';
		  $paragraphStack = '<p>';
		} else {
		  $paragraphStack = '</p><p>';
		}
	      }
	    } else {
	      if ( $paragraphStack ) {
		$output .= $paragraphStack;
		$paragraphStack = false;
		$this->mLastSection = 'p';
	      } else if ($this->mLastSection != 'p') {
		$output .= $this->closeParagraph().'<p>';
		$this->mLastSection = 'p';
	      }
	    }
	  }
	}
      }
      // somewhere above we forget to get out of pre block (bug 785)
      if($preCloseMatch && $this->mInPre) {
	$this->mInPre = false;
      }
      if ($paragraphStack === false) {
	$output .= $t."\n";
      }
    }
    while ( $prefixLength ) {
      $output .= $this->closeList( $pref2{$prefixLength-1} );
      --$prefixLength;
    }
    if ( '' != $this->mLastSection ) {
      $output .= '</' . $this->mLastSection . '>';
      $this->mLastSection = '';
    }
    
    return $output;
  }

  /**#@+
   * Used by doBlockLevels()
   * @access private
   */
  /* private */ function closeParagraph() {
    $result = '';
    if ( '' != $this->mLastSection ) {
      $result = '</' . $this->mLastSection  . ">\n";
    }
    $this->mInPre = false;
    $this->mLastSection = '';
    return $result;
  }
  // getCommon() returns the length of the longest common substring
  // of both arguments, starting at the beginning of both.
  //
  /* private */ function getCommon( $st1, $st2 ) {
    $fl = strlen( $st1 );
    $shorter = strlen( $st2 );
    if ( $fl < $shorter ) { $shorter = $fl; }
    
    for ( $i = 0; $i < $shorter; ++$i ) {
      if ( $st1{$i} != $st2{$i} ) { break; }
    }
    return $i;
  }
  // These next three functions open, continue, and close the list
  // element appropriate to the prefix character passed into them.
  //
  /* private */ function openList( $char ) {
    $result = $this->closeParagraph();
    
    if ( '*' == $char ) { $result .= '<ul><li>'; }
    else if ( '#' == $char ) { $result .= '<ol><li>'; }
    else if ( ':' == $char ) { $result .= '<dl><dd>'; }
    else if ( ';' == $char ) {
      $result .= '<dl><dt>';
      $this->mDTopen = true;
    }
    else { $result = '<!-- ERR 1 -->'; }
    
    return $result;
  }
  
  /* private */ function nextItem( $char ) {
    if ( '*' == $char || '#' == $char ) { return '</li><li>'; }
    else if ( ':' == $char || ';' == $char ) {
      $close = '</dd>';
      if ( $this->mDTopen ) { $close = '</dt>'; }
      if ( ';' == $char ) {
	$this->mDTopen = true;
	return $close . '<dt>';
      } else {
	$this->mDTopen = false;
	return $close . '<dd>';
      }
    }
    return '<!-- ERR 2 -->';
  }
  
  /* private */ function closeList( $char ) {
    if ( '*' == $char ) { $text = '</li></ul>'; }
    else if ( '#' == $char ) { $text = '</li></ol>'; }
    else if ( ':' == $char ) {
      if ( $this->mDTopen ) {
	$this->mDTopen = false;
	$text = '</dt></dl>';
      } else {
	$text = '</dd></dl>';
      }
    }
    else {	return '<!-- ERR 3 -->'; }
    return $text."\n";
  }
  /**#@-*/



}

/**
 * Escape html tags
 * Basicly replacing " > and < with HTML entities ( &quot;, &gt;, &lt;)
 *
 * @param string $in Text that might contain HTML tags
 * @return string Escaped string
 */
function wfEscapeHTMLTagsOnly( $in ) {
	return str_replace(
		array( '"', '>', '<' ),
		array( '&quot;', '&gt;', '&lt;' ),
		$in );
}

?>