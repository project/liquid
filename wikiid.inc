<?php

/*

  A wiki ID consists of a list of wiki names and namespaces

*/

class WikiId {

  var $data;

  function WikiId($data = null) {
    if ($data == null)
      $this->data = array();

    else if (is_array($data))
      $this->data = $data;

    else
      $this->data = $this->_parseString($data);
  }


  /* parses a wiki id and returns a WikiId object */
  function _parseString($str) {
    $list = explode('/', $str);
    $data = array();

    foreach ($list as $name) {
      $data[] = $this->_encodeName($name);
    }

    return $data;
  } 


  /* encodes a wiki name */
  function _encodeName($name) {
    return ucfirst(preg_replace('/ /','_', trim($this->_fixUTF8($name))));
  }

  /* decodes a wiki name*/
  function _decodeName($name) {
    return preg_replace('/_/', ' ', $name);
  }

  /* detects non UTF8 encoding and encodes the string to UTF8 if nessecary. */
  function _fixUTF8($str) {
    if (!preg_match('/[\x80-\xff]/', $str))
      return $str;
  
    if (preg_match('/^([\x00-\x7f]|[\xc0-\xdf][\x80-\xbf]|[\xe0-\xef][\x80-\xbf]{2}|'.
		   '[\xf0-\xf7][\x80-\xbf]{3})+$/', $str))
      return $str;
    
    return utf8_encode($str);
  }

  /* returns the string representation of the object (readable with parse method) */
  function toString() {
    return implode('/', $this->data);
  }

  function toTitleString() {
    $titles = array();
    foreach ($this->data as $name)
      $titles[] = $this->_decodeName($name);

    return implode('/', $titles);
  }

  function toURLString() {
    return implode('/', $this->data);
  }

  function identifier() {
    return implode('/', $this->data);
  }

  function name() {
    $e = end($this->data);
    return $e;
  }

  function displayName() {
    $e = end($this->data);
    return $this->_decodeName($e);
  }

  function parent() {
    $new = $this->data;
    array_pop($new);
    return new WikiId($new);
  }

  function data() {
    return $this->data;
  }

  function isNull() {
    return empty($this->data);
  }

  function isValid() {
    // data should of cause be an array...
    if (!is_array($this->data))
      return false;

    foreach ($this->data as $name) {
      // name may not be empty
      if ($name['name'] == '')
	return false;
    }

    return true;
  }

}